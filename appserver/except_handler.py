import sys

from controllers.robot_controller import robot


def stop_motors_except_handler(exctype, value, traceback):
    # on any exctype, stop motors
    print("Stopping all motors !")
    robot.stop_all()
    sys.__excepthook__(exctype, value, traceback)
