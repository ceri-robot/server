import threading

import serial


class __MoodController:
    IDLE = 0
    PICTURE = 1
    ANALYZE = 2
    LOGIN = 3
    LOGOUT = 4
    POST_ANALYZE = 5

    def __init__(self):
        super().__init__()
        self.ser = serial.Serial()
        self.ser.port = '/dev/serial/by-id/usb-Arduino__www.arduino.cc__0043_85430343038351904240-if00'
        self.ser.timeout = 5
        self.no_module = False

        try:
            if not self.ser.is_open:
                self.ser.open()
        except serial.SerialException:
            print("Unable to connect to MoodModule")
            self.no_module = True

        threading.Thread(target=self.read_thread, name="").start()

    def ask_mood(self, mood_id: int):
        if self.no_module or not self.ser.is_open:
            return

        self.ser.write(bytes([mood_id]))

    def read_thread(self):
        while not self.no_module and self.ser.is_open:
            line = str(self.ser.readline(), 'utf-8').strip()
            if not line == '':
                print("From MoodModule: {}".format(line))


mood = __MoodController()
