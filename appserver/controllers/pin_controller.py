import sys

if len(sys.argv) > 1 and sys.argv[1] == '-d':
    import __wiringpi_mock as wp
else:
    import wiringpi as wp

print("Initializing WiringPi")
wp.wiringPiSetup()
wp.piHiPri(0)
