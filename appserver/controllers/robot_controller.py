from controllers.pin_controller import wp


class __RobotController:
    SPD_MAX = 100

    SPD = 0
    IN1 = 1
    IN2 = 2

    FL = 0
    FR = 1
    BL = 2
    BR = 3

    MOTORS = [
        # FL
        [6, 10, 11, ],
        # FR
        [0, 2, 3, ],
        # BL
        [1, 4, 5, ],
        # BR
        [12, 14, 13, ],
    ]

    def __init__(self):
        self.motors_locked = False

    def reset(self):
        for motor in range(0, len(self.MOTORS)):
            wp.pinMode(self.MOTORS[motor][self.IN1], wp.OUTPUT)
            wp.digitalWrite(self.MOTORS[motor][self.IN1], 0)

            wp.pinMode(self.MOTORS[motor][self.IN2], wp.OUTPUT)
            wp.digitalWrite(self.MOTORS[motor][self.IN2], 0)

            wp.pinMode(self.MOTORS[motor][self.SPD], wp.OUTPUT)
            wp.softPwmCreate(self.MOTORS[motor][self.SPD], 0, self.SPD_MAX)

    def set_motor_status(self, motor, speed, direction):
        if speed == 0:
            self.stop(motor)

        is_forward = direction == 0

        if self.motors_locked and is_forward:
            self.stop_all()
            return

        wp.digitalWrite(self.MOTORS[motor][self.IN1], is_forward)
        wp.digitalWrite(self.MOTORS[motor][self.IN2], not is_forward)

        self.set_speed(motor, speed)

    def stop_all(self):
        for motor in range(0, len(self.MOTORS)):
            self.stop(motor)

    def stop(self, motor):
        self.set_speed(motor, 0)
        wp.digitalWrite(self.MOTORS[motor][self.IN1], 0)
        wp.digitalWrite(self.MOTORS[motor][self.IN2], 0)

    def speed_all(self, percent):
        for motor in range(0, len(self.MOTORS)):
            self.set_speed(motor, percent)

    def set_speed(self, motor, percent):
        wp.softPwmWrite(self.MOTORS[motor][self.SPD], percent)

    def lock_motors(self):
        self.stop_all()
        self.motors_locked = True
        print("Locked motors")

    def unlock_motors(self):
        self.motors_locked = False
        print("Unlocked motors")


print("Initializing RobotController")
robot = __RobotController()
robot.reset()
