import socketserver
import threading
from controllers.mood_controller import mood

HOST, PORT, VHOST, VPORT = "0.0.0.0", 1357, "0.0.0.0", 1358


def socket_message_listen_thread():
    print("Starting message server on {}:{}".format(HOST, PORT))
    from socket_handlers.socket_message_handler import SocketMessageHandler
    server = socketserver.TCPServer((HOST, PORT), SocketMessageHandler, False)
    server.allow_reuse_address = True
    try:
        server.server_bind()
        server.server_activate()
        server.serve_forever()
    except:
        server.server_close()
        raise


def socket_video_listen_thread():
    print("Starting video server on {}:{}".format(VHOST, VPORT))
    from socket_handlers.socket_video_handler import SocketVideoHandler
    server = socketserver.TCPServer((VHOST, VPORT), SocketVideoHandler, False)
    server.allow_reuse_address = True
    try:
        server.server_bind()
        server.server_activate()
        server.serve_forever()
    except:
        server.server_close()
        raise


def start_message_server():
    threading.Thread(target=socket_message_listen_thread, name="Socket message thread").start()
    print("Socket message started")


def start_video_server():
    threading.Thread(target=socket_video_listen_thread, name="Socket video thread").start()
    print("Socket video started")


def start_sensor_service():
    import services.ultrasonic_sensors as sensors
    threading.Thread(target=sensors.run, name="Ultrasonic sensors thread").start()
    print("Ultrasonic sensors thread started")


def start_tensorflow():
    print("Loading TF")
    from image_reco import image_reco
    print("Loading TF node lookup into memory...")
    image_reco.create_node_lookup()

    print("Loading TF graph into memory...")
    image_reco.create_graph()


if __name__ == '__main__':
    mood.ask_mood(mood.IDLE)
    # try:
    #     print("Trying to connect to debugger")
    #     import pydevd as pydevd
    #
    #     pydevd.settrace('localhost', port=56112, stdoutToServer=True, stderrToServer=True)
    # except:
    #     print("No debbuger")

    print("Hello MCP")
    # print("Setting up exception handler")
    # sys.excepthook = stop_motors_except_handler
    start_tensorflow()
    start_sensor_service()
    start_message_server()
    start_video_server()
