import sys
import time

from controllers.robot_controller import robot

if __name__ == '__main__':
    print("Using {}".format(sys.argv[1]))

    robot.speed_all(int(sys.argv[1]))
    for motorIndex in (robot.FL, robot.FR, robot.BL, robot.BR,):
        print(motorIndex)
        robot.forward(motorIndex)
        time.sleep(3)
        robot.stop(motorIndex)

    robot.stop_all()
