import socket
import socketserver
import struct
import threading
import time
from concurrent.futures import ThreadPoolExecutor
from io import BytesIO

import google.protobuf.message as Message

import encom.ImageRecognition_pb2 as ImageRecognition
import encom.MCP_pb2 as MCP
import encom.MovementControl_pb2 as MovementControl
from controllers.camera_controller import camera
from controllers.mood_controller import mood
from controllers.robot_controller import robot
from image_reco import image_reco

lock = threading.RLock()
NUM_WORKERS = 3


class SocketMessageHandler(socketserver.BaseRequestHandler):
    def __init__(self, request, client_address, server):
        self.executor = None
        super().__init__(request, client_address, server)

    def setup(self):
        print("Starting Executor with {} workers".format(NUM_WORKERS))
        self.executor = ThreadPoolExecutor(max_workers=NUM_WORKERS)

    def finish(self):
        mood.ask_mood(mood.LOGOUT)
        print("Shutting down Executor...")
        robot.stop_all()
        self.executor.shutdown()
        print("Executor stopped")

    def handle(self):
        mood.ask_mood(mood.LOGIN)
        print("New connection from {}".format(self.client_address[0]))
        while True:
            try:
                data = self.recv_size(self.request)
            except EOFError:
                print("Socket might be closed, dropping")
                return

            mcp_request = MCP.Request()
            mcp_request.ParseFromString(data)

            self.executor.submit(self.handle_request, mcp_request)

    def write_response(self, message: Message, request_id=0):
        with lock:
            response = MCP.Response()
            response.request_id = request_id
            response.message.Pack(message)

            print("size of message is {}".format(response.ByteSize()))

            self.write_message(self.request, response.SerializeToString())

    def handle_request(self, mcp_request: MCP.Request):
        request = MovementControl.MovementControlRequest()
        if mcp_request.message.Unpack(request):
            # print("is MovementControlRequest with {} messages".format(len(request.motors)))
            for motor in request.motors:
                # print("Motor: {}, Speed: {}, Direction: {}".format(motor.motor, motor.speed, motor.direction))
                robot.set_motor_status(motor.motor, motor.speed, motor.direction)
            return

        request = ImageRecognition.ImageRecognitionRequest()
        if mcp_request.message.Unpack(request):
            print('is ImageRecognitionRequest')

            mood.ask_mood(mood.PICTURE)

            ir_response = ImageRecognition.ImageRecognitionResponse()
            img_buffer = BytesIO()
            camera.resolution = (640, 480)
            camera.capture(img_buffer, 'jpeg', resize=(640, 480), thumbnail=None)
            ir_response.image = img_buffer.getvalue()
            ir_response.recognitionComplete = False

            self.write_response(ir_response, mcp_request.request_id)

            mood.ask_mood(mood.ANALYZE)

            print('Running image recognition')
            reco_start_time = time.clock()
            results = image_reco.run_inference_on_image(img_buffer.getvalue())
            reco_end_time = time.clock()
            print('image recognition finished, {} s'.format(reco_end_time - reco_start_time))
            ir_response = ImageRecognition.ImageRecognitionResponse()
            ir_response.recognitionComplete = True
            ir_response.image = img_buffer.getvalue()
            for (human_string, score) in results:
                print('found: {}: {}'.format(score, human_string))
                rec = ir_response.recognitionEntries.add()
                rec.human = human_string
                rec.score = score.item()

            mood.ask_mood(mood.POST_ANALYZE)

            self.write_response(ir_response, mcp_request.request_id)

            return

        print("Unknown message type...")

    @staticmethod
    def write_message(the_socket: socket, message: Message):
        size = len(message)
        size_data = struct.pack('!i', size)
        the_socket.send(size_data)
        send_size = the_socket.send(message)
        print("Sent {} of {}".format(send_size, size))

    @staticmethod
    def recv_size(the_socket: socket) -> str:
        size_data = the_socket.recv(4)
        if len(size_data) == 0:
            raise EOFError('No data to read')

        # print('size_data is {}'.format(len(size_data)))
        size = struct.unpack('!i', size_data)[0]
        # print('Reading for message of size {}'.format(size))

        return the_socket.recv(size)
