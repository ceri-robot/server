import io
import socket
import socketserver
import time

from controllers.camera_controller import camera


class SocketVideoHandler(socketserver.BaseRequestHandler):
    ENCODING = 'ASCII'

    def __init__(self, request, client_address, server):
        super().__init__(request, client_address, server)
        self.is_streaming = False

    def setup(self):
        super().setup()
        self.request.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, True)

    def finish(self):
        super().finish()
        self.is_streaming = False

    def handle(self):
        print("Video socket opened")
        connection = self.request.makefile('wb')  # type: io.BufferedWriter
        self.is_streaming = True
        connection.write(bytes("HTTP/1.0 200 OK\r\n", self.ENCODING))
        connection.write(bytes("Connection: close\r\n", self.ENCODING))
        connection.write(bytes("Server: MCP/0.1\r\n", self.ENCODING))
        connection.write(
            bytes("Cache-Control: no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0\r\n",
                  self.ENCODING))
        connection.write(bytes("Pragma: no-cache\r\n", self.ENCODING))
        connection.write(
            bytes("Content-Type: multipart/x-mixed-replace;boundary=boundarydonotcross\r\n", self.ENCODING))
        connection.write(bytes("\r\n", self.ENCODING))
        connection.flush()
        print("Video socket HTTP header sent")

        camera.resolution = (640, 480)

        stream = io.BytesIO()
        try:
            print("Video socket starting capture")
            for foo in camera.capture_continuous(stream, 'jpeg', use_video_port=True, resize=(640, 480),
                                                 thumbnail=None, quality=10, bayer=False):
                connection.write(bytes("--boundarydonotcross\r\n", self.ENCODING))
                connection.write(bytes("Content-Type: image/jpeg\r\n", self.ENCODING))
                connection.write(bytes("Content-Length: {}\r\n".format(len(stream.getvalue())), self.ENCODING))
                connection.write(bytes("\r\n", self.ENCODING))
                connection.write(stream.getvalue())
                connection.flush()
                stream.seek(0)
                stream.truncate()
                time.sleep(.5)
                if not self.is_streaming:
                    break
        except KeyboardInterrupt:
            pass
