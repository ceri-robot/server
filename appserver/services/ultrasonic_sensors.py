import statistics
import time

import config
from controllers.pin_controller import wp
from controllers.robot_controller import robot

TRIGGER_DURATION = 0.00001  # 10 us
ECHO_WAIT = TRIGGER_DURATION / 2
TIME_BETWEEN_CHECKS = 0.25


def poll_distance_for(echo_pin: int, trigger_pin: int):
    wp.digitalWrite(trigger_pin, wp.HIGH)
    time.sleep(TRIGGER_DURATION)
    wp.digitalWrite(trigger_pin, wp.LOW)

    safeloop = 0
    start = time.time()
    stop = start
    while wp.digitalRead(echo_pin) == wp.LOW and safeloop < 3000:
        safeloop += 1
        start = time.time()
        time.sleep(ECHO_WAIT)

    safeloop = 0
    while wp.digitalRead(echo_pin) == wp.HIGH and safeloop < 3000:
        safeloop += 1
        stop = time.time()
        time.sleep(ECHO_WAIT)

    elapsed = stop - start
    distance = (elapsed * 34000) / 2

    return elapsed, distance


def burst_poll_distance_for(echo_pin: int, trigger_pin: int, num_burst: int = 3):
    burst_result = []
    for b in range(0, num_burst):
        _, d = poll_distance_for(echo_pin, trigger_pin)
        burst_result.append(d)
        time.sleep(TRIGGER_DURATION * 2)

    return statistics.mean(burst_result)


def run():
    wp.pinMode(config.L_GPIO_TRIGGER, wp.OUTPUT)
    wp.pinMode(config.L_GPIO_ECHO, wp.INPUT)
    wp.pinMode(config.R_GPIO_TRIGGER, wp.OUTPUT)
    wp.pinMode(config.R_GPIO_ECHO, wp.INPUT)

    wp.digitalWrite(config.L_GPIO_TRIGGER, wp.LOW)
    wp.digitalWrite(config.R_GPIO_TRIGGER, wp.LOW)
    time.sleep(0.5)

    while True:
        l_d = 0  # burst_poll_distance_for(config.L_GPIO_ECHO, config.L_GPIO_TRIGGER)
        # time.sleep(TRIGGER_DURATION * 10)
        r_d = burst_poll_distance_for(config.R_GPIO_ECHO, config.R_GPIO_TRIGGER)

        if r_d < 5 or r_d > 80:
            continue

        if (r_d < 40) and not robot.motors_locked:
            print('[LOCK]   L: {:03.2f} R: {:03.2f}'.format(l_d, r_d))
            robot.stop_all()
            robot.lock_motors()
        elif (r_d >= 40) and robot.motors_locked:
            print('[UNLOCK] L: {:03.2f} R: {:03.2f}'.format(l_d, r_d))
            robot.unlock_motors()

        time.sleep(TIME_BETWEEN_CHECKS)
