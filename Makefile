all:
	@echo nop

upgrade-proto:
	git submodule update --remote 

update-proto:
	git submodule update 

proto:
	cd proto-interfaces && protoc --python_out=../appserver/encom *.proto

venv:
	rm -rf myvenv
	python3 -m venv myvenv

init:
	( \
	   source myvenv/bin/activate; \
	   pip3 install -r requirements.txt; \
	)

execute:
	( \
	   source myvenv/bin/activate; \
	   sudo python3 appserver; \
	)	

.PHONY: upgrade-proto update-proto proto venv init execute
